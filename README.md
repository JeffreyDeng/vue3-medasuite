<div align="center"><img width="200" src="https://gitee.com/chu1204505056/image/raw/master/logo/vab.svg" alt="VAB"/>
<h1> admin-plus</h1>
<h3> element-plus目前为测试版本，如追求稳定建议使用admin-pro开发正式项目，2021年6月19日起，plus稳定性已经上了一个台阶，如有新项目可以开始着手体验Admin plus</h3>
</div>

## 🔈 框架使用建议

- 1.使用前请一定先阅读 vip 群文档，一般在群公告前 5 条
- 2.如果您经过翻阅文档、百度后努力尝试仍无法解决问题，可通过 vip 群寻求帮助，讨论时间法定工作日 10 点-16 点
- 3.对于热心回答群内其他成员问题的用户，所提建议将优先被采纳，并可获得部分内测版本体验资格

## 🔈 框架使用约定

- 1.购买者可将授权后的产品用于任意「符合国家法律法规」的应用平台，禁止用于黄赌毒等危害国家安全与稳定的网站。
- 2.购买主体购买后可用于开发商业项目，不限制域名和项目数量，购买主体不可将源码分享第三方，否则我们有权利收回产品授权及更新权限，并根据事态轻重追究相应法律责任。
- 3.购买者务必尊重知识产权，严格保证不恶意传播产品源码、不得直接对授权的产品本身进行二次转售或倒卖、开源、不得对授权的产品进行简单包装后声称为自己的产品等，无论有意或无意，我们有权利收回产品授权及更新权限，并根据事态轻重追究相应法律责任。
- 4.购买者不可将 vip 群文档及资料分享给第三方，否则我们有权利收回产品授权及更新权限，并根据事态轻重追究相应法律责任。
- 5.购买者购买项目不可以用来构建存在竞争性质的产品并直接对外销售否则我们有权利收回产品授权及更新权限，并根据事态轻重追究相应法律责任。
- 6.购买者购买项目中的源码（包含全部源码、及部分源码片段）不可以用于任何形式的开源项目，否则我们有权利收回产品授权及更新权限，并根据事态轻重追究相应法律责任。
- 7.用于公司的项目商用时购买需提供公司名称，用于证明购买过我们的项目来用于商业用途，防范法律风险，我们不会将【购买公司】信息泄漏到互联网或告知第三方。
- 8.用于个人学习需提供姓名、联系方式。
- 9.如用于外包项目，购买者购买项目中的源码不可直接对外出售，npm run build 编译后的项目不受限制。
- 10.虚拟物品下单后不支持退货退款。
- 11.如无法遵守以上约定，请勿下单。
- 12.最终解释权归 vab 系列著作权人所有。

## 🔗 链接

- 💻 常规版演示地址：[admin-plus](https://chu1204505056.gitee.io/admin-plus/)
- 📝 使用文档：(文档地址及密码请查看 vip 群群公告第一条)
- 🗃 更新日志：[Releases](https://github.com/vue-admin-beautiful/admin-plus/releases)
- 📌 付费版及 vip 群购买地址：[购买地址](https://vue-admin-beautiful.com/authorization/)

## 🌱 版本

`master`分支更新频率较快，较为激进，不推荐直接使用

对于感兴趣的提交(commit)，可使用精选(Cherry-Pick)复制到自己的项目中

| 分支名                                                                      | 是否精简 commit | 是否精简功能 | 是否支持 i18n | 更新时间 |                                                                                                   维护人                                                                                                    |
| --------------------------------------------------------------------------- | :-------------: | :----------: | :-----------: | :------: | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------: |
| [master](https://github.com/vue-admin-beautiful/admin-plus/)                |       ❌        |      ❌      |       ✔       |   即时   |  <a href="https://github.com/chuzhixin" target="_blank"><img style="border-radius:999px" src="https://avatars3.githubusercontent.com/u/26647258?s=50&u=753921fb23f418996dffd6196e89729fcb2329ed&v=4"/></a>  |
| [template](https://github.com/vue-admin-beautiful/admin-plus/tree/template) |        ✔        |      ✔       |       ✔       | 10-30 天 | <a href="https://github.com/LiufengFish" target="_blank"><img style="border-radius:999px" src="https://avatars3.githubusercontent.com/u/29328241?s=50&u=bb0977b405ccf1a101ce4e18e4fb8d958854ca60&v=4"/></a> |
| [seed](https://github.com/vue-admin-beautiful/admin-plus/tree/seed)         |        ✔        |      ✔       |      ❌       | 10-30 天 | <a href="https://github.com/LiufengFish" target="_blank"><img style="border-radius:999px" src="https://avatars3.githubusercontent.com/u/29328241?s=50&u=bb0977b405ccf1a101ce4e18e4fb8d958854ca60&v=4"/></a> |

### 常规版各个使用请查阅 Vip 群文档

### 版权须知

Vab Admin 系列产品受国家计算机软件著作权保护（证书号：软著登字第 7051316 号），
禁止公开及传播产品源文件、二次出售等，
违者将承担相应的法律责任，并影响自身使用。

### TODO

目前暂时没有好的解决方案的几项待办事项，如果您有好的解决方案可以对仓库 【dev】 分支进行 pull request

- [ ] 由于 element 的菜单组件不支持设置超出自适应，所以导致横向布局时，菜单过多时处理不理想
- [ ] 头像上传组件浏览器 blob 转化异常

### 免费名额之外，额外加入 vip 群 （100/人 仅限已购买框架的的公司员工加入，购买后联系 微信 zxwk-hxq 即可）

[- 购买地址，网页右下角切换付款码即可](https://vue-admin-beautiful.com/authorization/)
